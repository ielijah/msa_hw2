Домашнее задание
----------------

Приложение в docker-образ и запушить его на Dockerhub

Цель:

Обернуть приложение в docker-образ и запушить его на Dockerhub.

  

Описание/Пошаговая инструкция выполнения домашнего задания:

**Шаг 1.** Создать минимальный сервис, который

1.  отвечает на порту 8000
2.  имеет http-метод:

*   GET /health/
*   RESPONSE: {"status": "OK"}  
      
    **Шаг 2.** Cобрать локально образ приложения в докер контенер под архитектуру AMD64.  
      
    Запушить образ в dockerhub  
      
    \*\*На выходе необходимо предоставить \*\*

1.  имя репозитория и тэг на Dockerhub
2.  ссылку на github c Dockerfile, либо приложить Dockerfile в ДЗ  
      
    _Обратите внимание_, что при сборке на m1 при запуске вашего контейнера на стандартных платформах будет ошибка такого вида:  
      
    _standard\_init\_linux.go:228: exec user process caued: exec format error_  
      
    Для сборки рекомендую указать тип платформы linux/amd64:  
      
    _docker build --platform linux/amd64 -t tag_  
      
    Более подробно можно прочитать в статье: [https://programmerah.com/how-to-solve-docker-run-error-standard\_init\_linux-go219-exec-user-process-caused-exec-format-error-39221/](https://programmerah.com/how-to-solve-docker-run-error-standard_init_linux-go219-exec-user-process-caused-exec-format-error-39221/ "https://programmerah.com/how-to-solve-docker-run-error-standard_init_linux-go219-exec-user-process-caused-exec-format-error-39221/")

  